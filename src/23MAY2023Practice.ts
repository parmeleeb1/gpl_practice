
//23MAY2023

// Task 1
// Create a typescript program that will figure out if a sentence is a pangram using a function

//----------------------------------------------------------

function checkPangram(sentence : string) : boolean {
    const letters : string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for(let i = 0; i < letters.length; i++) {
        if(sentence.toLowerCase().indexOf(letters[i]) === -1) return false;
    } return true;
}

// console.log(checkPangram("The quick brown fox jumps over a lazy dog."))
// console.log(checkPangram("This is not a pangram at all in the least."))

//----------------------------------------------------------
//----------------------------------------------------------

// Task 2
// create a function that will take a string as input. It should return an identical string with no  duplicate characters (capital and lowercase letters count as different from each other)

//----------------------------------------------------------

function removeDupes(sentence : string) : string {
    let newSentence : string = ''

    for(let i = 0; i < sentence.length;) {
        let letter : string = sentence.charAt(i)
        newSentence = newSentence.concat(letter)
        while(sentence !== sentence.replace(letter, '')) {
            sentence = sentence.replace(letter, '')
        }
    }
    return newSentence
}

//console.log(removeDupes("racecar"))
//console.log(removeDupes("Hello my friend, I am a froggie."))

//----------------------------------------------------------
//----------------------------------------------------------

// Task 3
// Variable review assignment
// /*
// declare 3 variables.
// One must be boolean and hold a value of false
// One must be a number and hold a value of 100
// One must be a string and hold a value of your first name
// Log each of these variables to the console. RUN THE PROGRAM, AND CONFIRM THAT THESE WERE LOGGED AS YOU EXPECTED
// Without changing the initial code, assign new values to the variables.
// The boolean will now hold true
// The number will now hold -99
// The string will now hold your last name. RUN THE PROGRAM AGAIN
//  */

//----------------------------------------------------------

function variables() {
    let trueOrFalse : boolean = false
    let numOfSomething : number = 100
    let firstName : string = 'brian'

    console.log("" + trueOrFalse + numOfSomething + firstName)

    trueOrFalse = true
    numOfSomething = -99
    firstName = 'parm'
}

//variables()